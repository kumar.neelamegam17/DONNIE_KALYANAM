package com.creativemindsz.donniekalyanam.CoreModules;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.creativemindsz.donniekalyanam.Constants.Constants;
import com.creativemindsz.donniekalyanam.Initialization.InitialActivity;
import com.creativemindsz.donniekalyanam.Others.ImageUploadInfo;
import com.creativemindsz.donniekalyanam.Others.UserDetails;
import com.creativemindsz.donniekalyanam.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.ZGrid;
import com.mzelzoghbi.zgallery.entities.ZColor;
import com.plattysoft.leonids.ParticleSystem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import static com.creativemindsz.donniekalyanam.CoreModules.General_Invitation.MyPREFERENCES;

public class Customized_Invitation extends AppCompatActivity {


    List<ImageUploadInfo> MainImageUploadInfoList;

    RelativeLayout ParentLayout;

    String Firebase_Identifier = "";
    String Str_Name = "";
    String Str_Mobile_Number = "";
    String Str_CustomMessage = "";
    String Str_ImageURL = "";


    ImageView ImageVw_Exit;
    FloatingActionButton response_fab;
    ImageView Friend_Photo;
    TextView TxtVw_Name, TxtVw_Mobile, TxtVw_CustomMessage;
    SharedPreferences sharedpreferences;
    ImageView Couples;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_customized_invitation);

        try {

            GETINITIALIZE();
            CONTROLLISTENERS();


        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


    private void GETINITIALIZE() {


        try {
            sharedpreferences = Customized_Invitation.this.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);

            if (sharedpreferences != null) {
                String id = sharedpreferences.getString(Constants.Friend_ID, null);
                String type = sharedpreferences.getString(Constants.InvitationType, null);

                Str_Name = sharedpreferences.getString(Constants.Name, null);
                Str_Mobile_Number = sharedpreferences.getString(Constants.Phone, null);
                Str_CustomMessage = sharedpreferences.getString(Constants.CustomMessage, null);
                Str_ImageURL = sharedpreferences.getString(Constants.Photo, null);
            }

            Friend_Photo = findViewById(R.id.profile_image);
            TxtVw_Name = findViewById(R.id.frnd_name);
            TxtVw_Mobile = findViewById(R.id.frnd_mobile);
            TxtVw_CustomMessage = findViewById(R.id.frnd_message);

            ParentLayout = findViewById(R.id.background_hook);
            ParentLayout.setBackgroundColor(Color.TRANSPARENT);


            Couples = findViewById(R.id.imgvw_couples);

            Couples.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    new ParticleSystem(Customized_Invitation.this, 100, R.drawable.heart, 1500)
                            .setSpeedRange(0.1f, 0.25f)
                            .oneShot(Couples, 100);


                }
            });

            try {
                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


                response_fab = findViewById(R.id.fab);
                YoYo.with(Techniques.Shake).duration(2500).repeat(5).playOn(findViewById(R.id.fab));


                ImageVw_Exit = findViewById(R.id.imgvw_exit);


            } catch (Exception e) {

                e.printStackTrace();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    protected void onStop() {
        super.onStop();

    }


    private void CONTROLLISTENERS() {


        try {

            if (Str_ImageURL.length() > 0) {

                Glide.with(Customized_Invitation.this).load(Str_ImageURL).into(Friend_Photo);

            } else {

                Glide.with(Friend_Photo.getContext())
                        .load(Uri.parse("file:///android_asset/male_avatar.png"))
                        .into(Friend_Photo);
            }

            TxtVw_CustomMessage.setText(Str_CustomMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            ImageVw_Exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Constants.LoadExit(Customized_Invitation.this);

                }
            });


            response_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new ParticleSystem(Customized_Invitation.this, 100, R.drawable.heart, 1500)
                            .setSpeedRange(0.1f, 0.25f)
                            .oneShot(view, 100);

                   /* if (Constants.isNetworkConnected(response_fab.getContext())) {
                        Customized_Invitation.this.finishAffinity();
                        Intent next = new Intent(Customized_Invitation.this, ScreenWeddingGalleryActivity.class);
                        Constants.LoadBack = "1";
                        startActivity(next);
                    }*/

                   LoadWeddingAlbums();


                }
            });


        } catch (Exception e) {

            e.printStackTrace();
        }


        Constants.LoadSnackbar(ParentLayout, "Click gallery icon to view wedding album");


    }






    ArrayList<String> imagesList;
    //database reference
    private DatabaseReference mDatabase;

    //progress dialog
    private ProgressDialog progressDialog;


    List<ImageLoadInfo> uploads = new ArrayList<>();

    private void LoadWeddingAlbums() {


        imagesList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);

        uploads = new ArrayList<>();

        //displaying progress dialog while fetching images
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        mDatabase = FirebaseDatabase.getInstance().getReference("wedding_album");

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressDialog.dismiss();

                //iterating through all the values in database
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    ImageLoadInfo upload = postSnapshot.getValue(ImageLoadInfo.class);
                    //uploads.add(upload);

                    imagesList.add(upload.photourl);

                }


                //creating adapter
                ///adapter = new LayoutImagesAdapter(getApplicationContext(), uploads);

                //adding adapter to recyclerview
                // recyclerView.setAdapter(adapter);

                /*  ZGallery.with(Customized_Invitation.this, imagesList)
                        .setToolbarTitleColor(ZColor.WHITE)
                        .setGalleryBackgroundColor(ZColor.WHITE)
                        .setToolbarColorResId(R.color.colorPrimary)
                        .setTitle("Donnie & Jenifer Wedding Album")
                        .show();*/

                ZGrid.with(Customized_Invitation.this, imagesList)
                        .setToolbarColorResId(R.color.colorPrimary)
                        .setTitle("Donnie & Jenifer Wedding Album")
                        .setToolbarTitleColor(ZColor.WHITE)
                        .setSpanCount(3)
                        .setGridImgPlaceHolder(R.color.colorPrimary)
                        .show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });



    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Constants.LoadExit(Customized_Invitation.this);
    }


}//ENDS
