package com.creativemindsz.donniekalyanam.CoreModules;

public class ImageLoadInfo {

    String name;
    String photourl;

    public ImageLoadInfo(String name, String photourl) {
        this.name = name;
        this.photourl = photourl;
    }

    public ImageLoadInfo()
    {

    }


    public String getName() {
        return name;
    }


    public String getPhotourl() {
        return photourl;
    }

}
