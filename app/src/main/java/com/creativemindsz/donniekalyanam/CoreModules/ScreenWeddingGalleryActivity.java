package com.creativemindsz.donniekalyanam.CoreModules;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.creativemindsz.donniekalyanam.Constants.Constants;
import com.creativemindsz.donniekalyanam.Utilities.LayoutImagesAdapter;
import com.creativemindsz.donniekalyanam.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.ZGrid;
import com.mzelzoghbi.zgallery.entities.ZColor;
import com.plattysoft.leonids.ParticleSystem;

import java.util.ArrayList;
import java.util.List;

public class ScreenWeddingGalleryActivity extends Activity {


    AppBarLayout appbar;
    ImageView imgvwExit;
    RecyclerView recyclerView;
    TextView textvwTitle;

    TextView Photography;

    //database reference
    private DatabaseReference mDatabase;

    //progress dialog
    private ProgressDialog progressDialog;


    List<ImageLoadInfo> uploads = new ArrayList<>();

    //adapter object
    RecyclerView.Adapter adapter;

    FloatingActionButton response_fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_wedding_gallery);

        try {
            GetInitialize();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void GetInitialize() {


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));





        Photography=findViewById(R.id.textvw_photography);
        Photography.setClickable(true);
        Photography.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='https://www.facebook.com/jenophotography2012/'> #JENO PHOTOGRAPHY - CONTACT : 9952397023 </a>";
        Photography.setText(Html.fromHtml(text));


        response_fab = findViewById(R.id.fab);
        YoYo.with(Techniques.Shake).duration(2500).repeat(2).playOn(findViewById(R.id.fab));

        response_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new ParticleSystem(ScreenWeddingGalleryActivity.this, 100, R.drawable.heart, 800)
                        .setSpeedRange(0.1f, 0.25f)
                        .oneShot(view, 100);

            }
        });


        ImageView Exit=findViewById(R.id.imgvw_gallery_exit);
        ImageView Back=findViewById(R.id.ic_back);


        Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.LoadExit(ScreenWeddingGalleryActivity.this);
            }
        });


        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        getAlbums();


    }

/*

    public void gridActivity(View v) {
        ZGrid.with(this, getDummyImageList())
                .setToolbarColorResId(R.color.colorPrimary)
                .setTitle("Zak Gallery")
                .setToolbarTitleColor(ZColor.WHITE)
                .setSpanCount(3)
                .setGridImgPlaceHolder(R.color.colorPrimary)
                .show();
    }


    public void galleryActivity(View v) {
        ZGallery.with(this, getDummyImageList())
                .setToolbarTitleColor(ZColor.WHITE)
                .setGalleryBackgroundColor(ZColor.WHITE)
                .setToolbarColorResId(R.color.colorPrimary)
                .setTitle("Zak Gallery")
                .show();
    }
*/


    ArrayList<String> imagesList;

    private void getAlbums() {

        imagesList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);

        uploads = new ArrayList<>();

        //displaying progress dialog while fetching images
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        mDatabase = FirebaseDatabase.getInstance().getReference("wedding_album");

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressDialog.dismiss();

                //iterating through all the values in database
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    ImageLoadInfo upload = postSnapshot.getValue(ImageLoadInfo.class);
                    //uploads.add(upload);

                    imagesList.add(upload.photourl);

                }


                //creating adapter
                ///adapter = new LayoutImagesAdapter(getApplicationContext(), uploads);

                //adding adapter to recyclerview
               // recyclerView.setAdapter(adapter);
                ZGallery.with(ScreenWeddingGalleryActivity.this, imagesList)
                        .setToolbarTitleColor(ZColor.WHITE)
                        .setGalleryBackgroundColor(ZColor.WHITE)
                        .setToolbarColorResId(R.color.colorPrimary)
                        .setTitle("Donnie❤Jenifer Wedding Album")
                        .show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });


    }


    @Override
    public void onBackPressed() {
        finish();

        if(Constants.LoadBack.equalsIgnoreCase("1"))
        {
            Intent next=new Intent(ScreenWeddingGalleryActivity.this, Customized_Invitation.class);
            startActivity(next);


        }else
        {
            Intent next=new Intent(ScreenWeddingGalleryActivity.this, General_Invitation.class);
            startActivity(next);

        }

    }
}
