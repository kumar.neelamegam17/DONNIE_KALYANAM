package com.creativemindsz.donniekalyanam.Utilities;


import android.Manifest;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v4.app.ActivityCompat;

import com.creativemindsz.donniekalyanam.R;


public abstract class CoreActivity extends RuntimePermissionsActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback {
    private Context context;
    private static final int REQUEST_PERMISSIONS = 25;


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
        setContext(this);
        setListeners();
    }

    /**
     * method to bind all views related to resourceLayout
     */
    protected abstract void bindViews();

    /**
     * called to set view listener for views
     */
    protected abstract void setListeners();

    //***************************************************************************************************
    public void isStoragePermissionGranted() {

        CoreActivity.super.requestAppPermissions(new
                        String[]{ Manifest.permission.INTERNET,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.GET_ACCOUNTS,

                }, R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);


    }
    //***************************************************************************************************
//End
}
