package com.creativemindsz.donniekalyanam.Utilities;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.creativemindsz.donniekalyanam.CoreModules.ImageLoadInfo;
import com.creativemindsz.donniekalyanam.R;

public class LayoutImagesAdapter extends RecyclerView.Adapter<LayoutImagesAdapter.ViewHolder> {

    private Context context;
    private List<ImageLoadInfo> uploads;

    public LayoutImagesAdapter(Context context, List<ImageLoadInfo> uploads) {
        this.uploads = uploads;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_images, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageLoadInfo upload = uploads.get(position);

        try {
            holder.textViewName.setText(upload.getName());


           // Glide.with(context).load(upload.getPhotourl()).thumbnail(0.25f).into(holder.imageView).onLoadFailed(context.getResources().getDrawable(R.drawable.noimage));

            Log.e("ImageName, URL: ", upload.getName()+" / "+upload.getPhotourl());

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return uploads.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewName;
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textViewName);
            imageView = itemView.findViewById(R.id.myZoomageView);
        }
    }
}

