package com.creativemindsz.donniekalyanam.Others;


public class ImageUploadInfo {
    public String getName() {
        return name;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public String getCustommessage() {
        return custommessage;
    }

    public String getPhotourl() {
        return photourl;
    }

    public String getComments() {
        return comments;
    }

    public String getFeedback() {
        return feedback;
    }

    private String name;
    private String mobilenumber;
    private String custommessage;
    private String photourl;
    private String comments;
    private String feedback;
    private String additional;
    private String firebaseid;

    public ImageUploadInfo(String name, String mobileNumber, String custmessage, String photourl, String comment, String feedback, String additional, String firebaseid) {

        this.name = name;
        this.mobilenumber = mobileNumber;
        this.custommessage = custmessage;
        this.photourl = photourl;
        this.comments = comment;
        this.feedback = feedback;
        this.additional = additional;
        this.firebaseid = firebaseid;

    }

    public ImageUploadInfo() {

    }


}
