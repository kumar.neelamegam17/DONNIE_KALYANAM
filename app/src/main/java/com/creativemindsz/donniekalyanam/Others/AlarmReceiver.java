package com.creativemindsz.donniekalyanam.Others;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.creativemindsz.donniekalyanam.Constants.Constants;
import com.creativemindsz.donniekalyanam.CoreModules.Customized_Invitation;
import com.creativemindsz.donniekalyanam.CoreModules.General_Invitation;
import com.creativemindsz.donniekalyanam.R;

import java.util.Random;

public class AlarmReceiver extends BroadcastReceiver {


    SharedPreferences sharedpreferences;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            sendNotification(context, Constants.Notification_Message, Constants.Notification_Title);
        } catch (Exception e) {
           // e.printStackTrace();
        }

    }

    private void sendNotification(Context context, String Message, String Title) {




        Message = "3 days more for Donnie Kalyanam..";
        Title = "Greetings - " + context.getString(R.string.app_name);

        Intent intent = null;

        sharedpreferences = context.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences != null) {
            String id = sharedpreferences.getString(Constants.Friend_ID, null);
            String type = sharedpreferences.getString(Constants.InvitationType, null);

            Log.e("Id/Type: ", id + " / " + type);

            if (id != null && id.length() > 0) {

                if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_NORMAL)) {


                    intent = new Intent(context, General_Invitation.class);

                } else if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_CUSTOM)) {

                    intent = new Intent(context, Customized_Invitation.class);

                }


            }


        }


        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, new Random().nextInt(100), intent, PendingIntent.FLAG_ONE_SHOT);
        long when = System.currentTimeMillis();

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(context);
        mNotifyBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000, 1000});
        boolean lollipop = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);


        if (lollipop) {

            mNotifyBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle(Title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(Message))
                    .setContentText(Message)
                    .setColor(Color.TRANSPARENT)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.wedding_couple_small))
                    .setSmallIcon(R.drawable.ic_action_notify)
                    .setWhen(when).setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

        } else {

            mNotifyBuilder = new NotificationCompat.Builder(context)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(Message))
                    .setContentTitle(Title)
                    .setContentText(Message)
                    .setSmallIcon(R.drawable.ic_action_notify)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.wedding_couple_small))
                    .setWhen(when).setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt(100) /* ID of notification */, mNotifyBuilder.build());


    }
/*
    private void ShowNotification(Context context, String Message, String Title) {

        Message = "3 Days more to Donnie Kalyanam.. Stay tuned..";
        Title = "Greetings";

        PendingIntent pi = null;
        sharedpreferences = context.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences != null) {
            String id = sharedpreferences.getString(Constants.Friend_ID, null);
            String type = sharedpreferences.getString(Constants.InvitationType, null);

            Log.e("Id/Type: ", id + " / " + type);

            if (id != null && id.length() > 0) {

                if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_NORMAL)) {

                    pi = PendingIntent.getActivity(context, 0, new Intent(context, General_Invitation.class), 0);

                } else if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_CUSTOM)) {

                    pi = PendingIntent.getActivity(context, 0, new Intent(context, Customized_Invitation.class), 0);

                }


            }


        }


        Uri notificationS = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);
        } else {

            Notification notification = new NotificationCompat.Builder(context)
                    .setTicker("Donnie Kalyanam - Notification")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(Title)
                    .setContentText(Message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(Message))
                    .setContentIntent(pi)
                    .setAutoCancel(true)
                    .setSound(notificationS)
                    .build();
            notification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);

        }


    }*/


}//END
