package com.creativemindsz.donniekalyanam.Constants;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.creativemindsz.donniekalyanam.R;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

public class Constants {


    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Donnie Kalyanam";
    public static final String CHANNEL_DESCRIPTION = "My Marriage Invitation";
    public static String EVENT_DATE_TIME = "2018-04-22 10:00:00";
    public static String Storage_Path = "Friends_Photos/";
    public static String Database_Path = "donnie-kalyanam";
    public static Double Location_Latitude = 10.603118;
    public static Double Location_Longitude = 78.417100;

    public static String INVITATION_TYPE_NORMAL = "NORMAL";
    public static String INVITATION_TYPE_CUSTOM = "CUSTOM";


    public static final String Friend_ID = "IdKey";
    public static final String Name = "nameKey";
    public static final String Phone = "phoneKey";
    public static final String Photo = "phoneKey";
    public static final String InvitationType = "typeKey";
    public static final String CustomMessage = "custommessageKey";
    public static final String Comments = "commentsKey";
    public static final String Feedback = "feedbackKey";

    public static final String  Response_Flag = "Response_Flag";
    public static final String  Feedback_Flag = "Feedback_Flag";


    public static final String  FirebaseToken_Str = "";


    public static String Notification_Message="";
    public static String Notification_Title="";


    public static String FirebaseToken="FirebaseToken";





    public static final String MyPREFERENCES = "MyPrefs" ;
    public static String LoadBack="1";

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean ret = cm.getActiveNetworkInfo() != null;

        if (!ret) {
            Toast.makeText(ctx, "No data connection available.\nEnable data connection.. try again later...", Toast.LENGTH_SHORT).show();
        }

        return ret;
    }

    public static void LoadExit(final Activity ctx) {
        new FancyAlertDialog.Builder(ctx)
                .setTitle("Information")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Are you sure want to exit?")
                .setNegativeBtnText("NO")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("YES")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SLIDE)
                .isCancellable(false)
                .setIcon(R.drawable.ic_exit_to_app_black_24dp, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                        ctx.finishAffinity();
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {


                    }
                })
                .build();
    }



    public static void LoadSnackbar(View vw, String Message)
    {

        try {
            Snackbar snack = Snackbar.make(vw, Message, Snackbar.LENGTH_LONG);
            View view = snack.getView();
            TextView tv = view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snack.show();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }



}













