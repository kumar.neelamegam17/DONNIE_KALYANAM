package com.creativemindsz.donniekalyanam.FireBase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.creativemindsz.donniekalyanam.Constants.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {



    SharedPreferences sharedpreferences;

    public static final String MyPREFERENCES = "MyPrefs";
    //this method will be called
    //when the token is generated
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();

        //for now we are displaying the token in the log
        //copy it as this method is called only when the new token is generated
        //and usually new token is only generated when the app is reinstalled or the data is cleared
        Log.e("MyRefreshedToken", token);
        Constants.FirebaseToken=token;

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.FirebaseToken_Str, token);
        editor.commit();

    }
}
