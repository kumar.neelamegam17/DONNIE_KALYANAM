package com.creativemindsz.donniekalyanam.Initialization;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativemindsz.donniekalyanam.Constants.Constants;
import com.creativemindsz.donniekalyanam.CoreModules.Customized_Invitation;
import com.creativemindsz.donniekalyanam.CoreModules.General_Invitation;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver2;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver3;
import com.creativemindsz.donniekalyanam.Others.ImageUploadInfo;
import com.creativemindsz.donniekalyanam.Others.UserDetails;
import com.creativemindsz.donniekalyanam.R;
import com.creativemindsz.donniekalyanam.Utilities.CoreActivity;
import com.creativemindsz.donniekalyanam.Utilities.Validation1;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.plattysoft.leonids.ParticleSystem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class InitialActivity extends CoreActivity {


    final static int RQS_1 = 1;
    DatabaseReference databaseReference;
    List<ImageUploadInfo> list = new ArrayList<>();
    CardView cardvwMainContent;
    EditText edtitextMobilenumber;
    Button Submit;
    ImageView Background;
    ImageView ImageVw_Exit;
    ConstraintLayout ParentLayout;
    SharedPreferences sharedpreferences;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_initialization);


        try {
            isStoragePermissionGranted();
        } catch (Exception e) {
            //e.printStackTrace();
        }


    }

    @Override
    public void onPermissionsGranted(int requestCode) {

        sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences != null) {
            String id = sharedpreferences.getString(Constants.Friend_ID, null);
            String type = sharedpreferences.getString(Constants.InvitationType, null);

            Log.e("Id/Type: ", id + " / " + type);

            if (id != null && id.length() > 0) {

                if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_NORMAL)) {
                    this.finishAffinity();
                    Intent next = new Intent(InitialActivity.this, General_Invitation.class);
                    startActivity(next);
                } else if (type != null && type.equalsIgnoreCase(Constants.INVITATION_TYPE_CUSTOM)) {
                    this.finishAffinity();
                    Intent next = new Intent(InitialActivity.this, Customized_Invitation.class);
                    startActivity(next);
                }


            }
        }


        try {

            GETINITIALIZE();
            CONTROLLISTENERS();

        } catch (Exception e) {
            //  e.printStackTrace();
        }

    }

    private void GETINITIALIZE() {

        try {

            databaseReference = FirebaseDatabase.getInstance().getReference(Constants.Database_Path);
            cardvwMainContent = findViewById(R.id.cardvw_main_content);
            edtitextMobilenumber = findViewById(R.id.edtitext_mobilenumber);
            Submit = findViewById(R.id.button_submit);
            Background = findViewById(R.id.imgvw_bg);
            YoYo.with(Techniques.FadeInUp).duration(1500).playOn(cardvwMainContent);
            edtitextMobilenumber.setText("");
            ImageVw_Exit = findViewById(R.id.imgvw_exit);
            ParentLayout = findViewById(R.id.parent_init);


        } catch (Exception e) {
            //e.printStackTrace();
        }

    }


    @Override
    public void onBackPressed() {

    }

    private void CONTROLLISTENERS() {


        ImageVw_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.LoadExit(InitialActivity.this);

            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    ShowPreparingDialog();


                    new ParticleSystem(InitialActivity.this, 1000, R.drawable.ic_stars, 500).setSpeedRange(
                            0.1f, 0.25f).oneShot(view, 100);

                    if (Constants.isNetworkConnected(Submit.getContext())) {
                        //if (Validation1.isPhoneNumber(edtitextMobilenumber, false)) {
                        if (edtitextMobilenumber.getText().length() > 0) {

                            if (edtitextMobilenumber.getText().toString().length() == 10 || edtitextMobilenumber.getText().toString().length() == 13 || edtitextMobilenumber.getText().toString().length() == 15) {
                                String Str_Mobile_Number = edtitextMobilenumber.getText().toString();

                                dialog.show();

                                GetFriendsDetails(Str_Mobile_Number);


                            } else {

                                Constants.LoadSnackbar(ParentLayout, "Enter valid mobile number..");
                            }


                        } else {

                            Constants.LoadSnackbar(ParentLayout, "Enter your mobile number..");
                        }
                    }
                } catch (Exception e) {
                    //   e.printStackTrace();
                }


            }
        });

    }

    private void ShowPreparingDialog() {

        dialog = new Dialog(InitialActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.progress_layout);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView title = dialog.findViewById(R.id.title);
        TextView content = dialog.findViewById(R.id.content);
        ImageView icon = dialog.findViewById(R.id.icon);

        YoYo.with(Techniques.RubberBand).repeat(5).duration(1500).playOn(icon);


        title.setText(R.string.please_wait);
        content.setText(R.string.personal_invitation);

        dialog.getWindow().setAttributes(lp);

    }


    private void GetFriendsDetails(final String mobileNumber) {


        try {


            Query query = databaseReference.orderByChild("mobilenumber").equalTo(mobileNumber).limitToFirst(1);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    try {
                        if (dataSnapshot.exists()) {

                            String friend_NAME = "";
                            String friend_MOBILENUMBER = "";
                            String custom_MESSAGE = "";
                            String photo_URL = "";
                            String UniqueId = "";


                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                friend_NAME = (String) postSnapshot.child("name").getValue();
                                friend_MOBILENUMBER = (String) postSnapshot.child("mobilenumber").getValue();
                                custom_MESSAGE = (String) postSnapshot.child("custommessage").getValue();
                                photo_URL = (String) postSnapshot.child("photourl").getValue();
                                UniqueId = postSnapshot.getKey();
                            }


                            dialog.dismiss();

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Constants.Friend_ID, UniqueId);
                            editor.putString(Constants.Name, friend_NAME);
                            editor.putString(Constants.Phone, friend_MOBILENUMBER);
                            editor.putString(Constants.Photo, photo_URL);
                            editor.putString(Constants.InvitationType, Constants.INVITATION_TYPE_CUSTOM);
                            editor.putString(Constants.CustomMessage, custom_MESSAGE);
                            editor.putString("FirebaseToken", Constants.FirebaseToken);
                            editor.commit();

                            Intent invite = new Intent(InitialActivity.this, Customized_Invitation.class);
                            startActivity(invite);
                            finish();


                        } else {

                            UserDetails userinfo = new UserDetails();
                            Log.e("User Info: ", userinfo.getDetails());

                            String email = "NA";

                            Pattern gmailPattern = Patterns.EMAIL_ADDRESS;
                            Account[] accounts = AccountManager.get(InitialActivity.this).getAccounts();
                            for (Account account : accounts) {
                                if (gmailPattern.matcher(account.name).matches()) {
                                    email = account.name;
                                }
                            }

                            Log.e("Email Id: ", email);


                            dialog.dismiss();

                            ShowPreparingDialog();

                            // Adding image upload id s child element into databaseReference.
                            String NormalId = databaseReference.push().getKey();

                            sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Constants.Friend_ID, NormalId);
                            editor.putString(Constants.Name, email + "\n" + userinfo.toString());
                            editor.putString(Constants.Phone, edtitextMobilenumber.getText().toString());
                            editor.putString(Constants.Photo, "https://firebasestorage.googleapis.com/v0/b/donnie-kalyanam.appspot.com/o/Friends_Photos%2Fuser.png?alt=media&token=aad63999-ad20-48d3-9087-77bc3ebcaf54");
                            editor.putString(Constants.InvitationType, Constants.INVITATION_TYPE_NORMAL);
                            editor.putString(Constants.CustomMessage, "");
                            editor.putString("FirebaseToken", Constants.FirebaseToken);
                            editor.commit();

                            String pic = "https://firebasestorage.googleapis.com/v0/b/donnie-kalyanam.appspot.com/o/Friends_Photos%2Fuser.png?alt=media&token=aad63999-ad20-48d3-9087-77bc3ebcaf54";
                            ImageUploadInfo imageUploadInfo = new ImageUploadInfo(email + "\n" + userinfo, edtitextMobilenumber.getText().toString(),
                                    "Hello Friend!", pic, "-", "-", "-", Constants.FirebaseToken);

                            databaseReference.child(NormalId).setValue(imageUploadInfo);

                            dialog.dismiss();

                            //Snackbar.make(ParentLayout, "Sorry for inconvenience..Your mobile number match not found with our database..", Snackbar.LENGTH_LONG).show();
                            Constants.LoadSnackbar(ParentLayout, "Sorry for inconvenience..Your mobile number match not found with our database..");
                            startActivity(new Intent(InitialActivity.this, General_Invitation.class));
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    dialog.dismiss();

                }
            });
        } catch (Exception e) {
            //  e.printStackTrace();
        }
    }

    @Override
    protected void bindViews() {

    }

    @Override
    protected void setListeners() {

    }


}//ENDS