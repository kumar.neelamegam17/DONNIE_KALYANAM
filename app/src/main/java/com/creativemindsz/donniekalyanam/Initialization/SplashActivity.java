package com.creativemindsz.donniekalyanam.Initialization;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.creativemindsz.donniekalyanam.FireBase.MyNotificationManager;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver2;
import com.creativemindsz.donniekalyanam.Others.AlarmReceiver3;
import com.creativemindsz.donniekalyanam.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.Calendar;
import java.util.Date;

public class SplashActivity extends AppCompatActivity {


    private ConstraintLayout linearLayout;
    private ImageView imageviewTopbanner;
    private ImageView imageviewBottombanner;
    private ImageView imageviewLogo;
    private TextView textvwTitle;


    ProgressBar progressbar1;
    int intValue = 0;
    Handler handler = new Handler();
    SharedPreferences prefs = null;


    final static int RQS_1 = 1;

    final static int RQS_2 = 2;

    final static int RQS_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);


        try {


            GETINITIALIZE();

            CONTROLLISTENERS();


        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    private void CONTROLLISTENERS() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (intValue < 100) {
                    intValue += 5;

                    handler.post(new Runnable() {

                        @Override
                        public void run() {


                            progressbar1.setProgress(intValue);


                        }
                    });
                    try {
                        Thread.sleep(300);


                    } catch (InterruptedException e) {
                       // e.printStackTrace();
                    }
                }

                startActivity(new Intent(SplashActivity.this, InitialActivity.class));
                finish();

            }
        }).start();


    }


    private void GETINITIALIZE() {


        try {
            imageviewTopbanner = findViewById(R.id.imageview_topbanner);
            imageviewBottombanner = findViewById(R.id.imageview_bottombanner);
            imageviewLogo = findViewById(R.id.imageview_logo);
            textvwTitle = findViewById(R.id.textvw_title);
            progressbar1 = findViewById(R.id.progressBar3);

            //YoYo.with(Techniques.Landing).duration(1500).playOn(imageviewTopbanner);

            //YoYo.with(Techniques.Landing).duration(1500).playOn(imageviewBottombanner);

            YoYo.with(Techniques.ZoomInUp).duration(1600).playOn(findViewById(R.id.imageview_logo));
            YoYo.with(Techniques.Pulse).duration(1500).playOn(findViewById(R.id.textvw_title));

            progressbar1.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

            prefs = getSharedPreferences("first_time", MODE_PRIVATE);


            //LoadAlarm_3Days();
            //LoadAlarm_2Days();
            //LoadAlarm_1Days();


        } catch (Exception e) {
          //  e.printStackTrace();
        }


    }




    private void LoadAlarm_3Days() {

        try {

            Calendar cal = Calendar.getInstance();
            cal.set(2018, 03, 18, 10, 30, 00);
            Date current_date = new Date();
            if (!current_date.after(cal.getTime())) {

                Log.e("Alarm 1", "\n\n***\n" + "Alarm is set@ " + cal.getTime() + "\n" + "***\n");
                Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

            }

        } catch (Exception e) {
            //e.printStackTrace();
        }

    }


    private void LoadAlarm_2Days() {

        try {

            Calendar cal = Calendar.getInstance();
            cal.set(2018, 03, 20, 10, 30, 00);
            Date current_date = new Date();
            if (!current_date.after(cal.getTime())) {

                Log.e("Alarm 2", "\n\n***\n" + "Alarm is set@ " + cal.getTime() + "\n" + "***\n");
                Intent intent = new Intent(getBaseContext(), AlarmReceiver2.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

            }

        } catch (Exception e) {
            //e.printStackTrace();
        }

    }

    private void LoadAlarm_1Days() {

        try {

            Calendar cal = Calendar.getInstance();
            cal.set(2018, 03, 21, 10, 30, 00);
            Date current_date = new Date();
            if (!current_date.after(cal.getTime())) {

                Log.e("Alarm 3", "\n\n***\n" + "Alarm is set@ " + cal.getTime() + "\n" + "***\n");
                Intent intent = new Intent(getBaseContext(), AlarmReceiver3.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_3, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            }

        } catch (Exception e) {
            //e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstrun", true)) {
            MyNotificationManager.getInstance(this).displayNotification("Greetings", "Welcome to donnie kalyanam, Thanks for installing app.");

            prefs.edit().putBoolean("firstrun", false).commit();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}//ENDS